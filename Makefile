ENV=./env
BIN=${ENV}/bin
PYTHON=${BIN}/python
PIP=${BIN}/pip
BLACK=${BIN}/black src setup.py tests

all: virtual run

virtual:
	python3 -m virtualenv ${ENV}
	${PIP} install -r requirements.txt
	${PIP} install -r dev_requirements.txt
	${PYTHON} setup.py develop

run:
	${PYTHON} -m productapi.main

black:
	$(BLACK)

black_test:
	$(BLACK) --check

mypy:
	${BIN}/mypy src

pytest:
	${BIN}/pytest --cov=src tests

test: black_test mypy pytest

docker:
	docker build -t productapi:latest .

run_docker: docker
	docker run -p 5000:5000 productapi:latest

clean:
	rm -r env || echo "Nothing to remove"
	rm -r src/*.egg-info || echo "Nothing to remove"
	rm -r .mypy_cache || echo "Nothing to remove"
	rm -r .pytest_cache || echo "Nothing to remove"
	rm .coverage || echo "Nothing to remove"