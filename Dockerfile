FROM python:3.8-alpine

WORKDIR /var/task

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt --no-cache-dir

COPY src src
COPY setup.py setup.py
COPY README.md README.md

RUN python setup.py install

EXPOSE 5000

ENTRYPOINT gunicorn 'productapi.main:main()' --bind=0.0.0.0:5000