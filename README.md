# Requirements

## Local

To run locally you need:

* `Make`
* `Python 3.8`
* `Virtualenv`

## Prod-like

To run more-prod like environment all you need is `docker`

# Local Environment

To set up local environment just run `make`.

To run environment without local set up run `make run_docker`

## Make commands

* `virtual` - setup a virtual environment or update it
* `run` - run local dev server
* `black` - run black auto-formatter
* `black_test` - run black in check mode
* `mypy` - run mypy check against src
* `pytest` - run all python tests
* `test` - run black_test, mypy and pytest
* `docker` - build docker container
* `run_docker` - build and run docker container
* `clean` - delete local cache files

# API

## Add new product

POST `/`

JSON Body:

* `name`: `str`
* `sku`: `str`
* `qty`: Optional `int` (Default `0`)
* `price`: `float`

Example:
```bash
curl -XPOST http://localhost:5000/ --data '{"name": "Foo Inc", "sku": "newSKU", "price": 12}' -H 'Content-Type: application/json'
```

## Get list of all products

GET `/`

Arguments:

* `filter`: Optional `available` (qty > 0) or `sold_out` (qty = 0)

Example:
```bash
curl -XGET http://localhost:5000/?filter=sold_out
```

## Get Product

GET `/<string:sku>`

Example:
```bash
curl -XGET http://localhost:5000/newSKU
```

## Update Product

PUT `/<string:sku>`

JSON Body:

* `name`: Optional `str`
* `qty`: Optional `int`
* `price`: Optional `float`

Example:
```bash
curl -XPUT http://localhost:5000/newSKU --data '{"price": 15.5, "qty": 100}' -H 'Content-Type: application/json'
```