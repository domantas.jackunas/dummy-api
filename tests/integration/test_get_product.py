from productapi.models import data_store


def test_get_product_by_sku(client, list_of_products):
    sku = "sku1"

    response = client.get(f"/{sku}")
    assert response.status_code == 404

    data_store.update(list_of_products)
    response = client.get(f"/{sku}")
    assert response.status_code == 200
    assert response.json == list_of_products[sku]
