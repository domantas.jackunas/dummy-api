import copy

from productapi.models import data_store


def test_update_product(client, list_of_products):
    product = copy.deepcopy(list_of_products["sku1"])
    update = {"name": "new_name", "qty": 13, "price": 100}

    assert client.put(f"/{product['sku']}", json=update).status_code == 404

    data_store.update(list_of_products)

    assert client.put(f"/{product['sku']}", json=update).status_code == 200

    product.update(update)
    response = client.get(f"/{product['sku']}")
    assert response.json == product


def test_update_product_failures(client, list_of_products):
    data_store.update(list_of_products)

    assert client.put(f"/sku1", json={"qty": -1}).status_code == 400
    assert client.put(f"/sku1", json={"price": -1}).status_code == 400
