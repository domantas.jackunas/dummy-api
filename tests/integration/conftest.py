import pytest

from productapi.main import main
from productapi.models import data_store


@pytest.fixture
def client():
    app = main()
    with app.test_client() as client:
        yield client


@pytest.fixture
def list_of_products():
    return {
        "sku1": {"name": "a", "sku": "sku1", "qty": 0, "price": 3.50},
        "sku2": {"name": "b", "sku": "sku2", "qty": 1, "price": 4.50},
        "sku3": {"name": "v", "sku": "sku3", "qty": 2, "price": 5.50},
    }


@pytest.fixture(autouse=True)
def cleanup():
    yield  # only need to run clear after the test is executed
    data_store.clear()
