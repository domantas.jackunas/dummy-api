from productapi.models import data_store


def test_empty_list_products(client):
    response = client.get("/")
    assert response.status_code == 200
    assert response.json == {"products": []}


def test_list_products(list_of_products, client):
    data_store.update(list_of_products)

    response = client.get("/")
    assert response.status_code == 200
    assert response.json == {"products": list(list_of_products.values())}


def test_list_products_filter_sold_out(list_of_products, client):
    data_store.update(list_of_products)

    response = client.get("/?filter=sold_out")
    assert response.status_code == 200
    assert len(response.json["products"]) == 1


def test_list_products_filter_available(list_of_products, client):
    data_store.update(list_of_products)

    response = client.get("/?filter=available")
    assert response.status_code == 200
    assert len(response.json["products"]) == 2
