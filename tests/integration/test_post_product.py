def test_create_new_product(client):
    new_product = {
        "name": "name",
        "sku": "New SKU",
        "price": 0,
    }

    response = client.post("/", json=new_product)
    assert response.status_code == 200

    # 2nd attempt must fail as SKU is registered
    response = client.post("/", json=new_product)
    assert response.status_code == 400
