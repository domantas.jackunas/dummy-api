from unittest.mock import patch

from pytest import raises

from productapi.models import Product, NewProduct


def test_product_registration_object():
    assert Product(name="a", sku="a", qty=0, price=0)
    assert Product(name="a", sku="a", price=0)
    assert Product(name="Foo Inc", sku="1asd1321erwer", qty=13, price=13)

    with raises(ValueError):
        assert Product(name="a", sku="a")
        assert Product(name="a", price=0)
        assert Product(sku="a", price=0)


def test_product_registration_object_gte_validator():
    with raises(ValueError):
        assert Product(name="a", sku="a", price=-1)
        assert Product(name="a", sku="a", qty=-1, price=1)


@patch("productapi.models.data_store")
def test_product_registration_object_existing_sku(data_store_mock):
    data_store_mock.__contains__ = {"old_sku": {}}
    with raises(ValueError):
        NewProduct(name="a", sku="old_sku", price=0)
