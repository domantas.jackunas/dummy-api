from pytest import raises

from productapi.models import ProductListQuery


def test_product_query_object():
    assert ProductListQuery(filter="available")
    assert ProductListQuery(filter="sold_out")
    assert ProductListQuery()

    with raises(ValueError):
        assert ProductListQuery(filter="does_not_exist")
