from enum import Enum
from typing import Optional, Dict, List

from pydantic import BaseModel, validator


data_store: Dict[str, dict] = {}


def must_be_gte_than_zero(value):
    if value < 0:
        raise ValueError("Value must be 0 or more")
    return value


class Product(BaseModel):
    sku: str
    name: str
    qty: Optional[int] = 0
    price: float

    _must_be_gte_than_zero_qty_price = validator("qty", "price", allow_reuse=True)(
        must_be_gte_than_zero
    )


class NewProduct(Product):
    @validator("sku")
    def unique_sku(cls, value):
        print(f"{data_store=}, {value=}")
        if value in data_store:
            raise ValueError("SKU already exists")
        return value


class ProductList(BaseModel):
    products: List[Product]


class FilterEnum(str, Enum):
    available = "available"
    sold_out = "sold_out"


class ProductListQuery(BaseModel):
    filter: Optional[FilterEnum]


class ProductUpdate(BaseModel):
    name: Optional[str]
    qty: Optional[int]
    price: Optional[float]

    _must_be_gte_than_zero_qty_price = validator("qty", "price", allow_reuse=True)(
        must_be_gte_than_zero
    )
