from flask import Flask


app = Flask(__name__)


def main():
    from productapi.routes import blueprint as product_blueprint

    app.register_blueprint(product_blueprint)

    return app


if __name__ == "__main__":
    main().run()
