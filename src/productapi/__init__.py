# Imports to avoid circular import issue
from productapi.main import app
from productapi.routes import list_products, register_product
