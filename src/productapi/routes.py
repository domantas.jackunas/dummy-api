from flask import request, Blueprint
from flask_pydantic import validate  # type: ignore

from productapi.models import (
    data_store,
    Product,
    NewProduct,
    ProductList,
    ProductListQuery,
    ProductUpdate,
)

blueprint = Blueprint("product", __name__)


@blueprint.route("/", methods=["POST"])
@validate(body=NewProduct)
def register_product() -> Product:
    new_product_data = request.json
    data_store[new_product_data["sku"]] = new_product_data
    return Product(**new_product_data)


@blueprint.route("/", methods=["GET"])
@validate(query=ProductListQuery)
def list_products() -> ProductList:
    products = []
    list_filter = request.args.get("filter")

    for sku, product_data in data_store.items():
        if list_filter == "sold_out" and product_data["qty"] != 0:
            continue

        if list_filter == "available" and product_data["qty"] == 0:
            continue

        products.append(Product(**product_data))
    return ProductList(products=products)


@blueprint.route("/<string:sku>", methods=["GET"])
@validate()
def list_product(sku):
    if sku not in data_store:
        return "not found", 404

    return Product(**data_store[sku])


@blueprint.route("/<string:sku>", methods=["PUT"])
@validate(body=ProductUpdate)
def update_product(sku):
    if sku not in data_store:
        return "not found", 404

    data_store[sku].update(request.json)

    return Product(**data_store[sku])
